
# -*- coding: utf-8 -*-
{
    # Display name for module
    'name': "Employee",

    # A brief summary of the module
    'summary': """Employee Management""",

    # Description for the module
    'description': """
        Employee module for managing:
            - attendance 
            - salary
            - leave
    """,

    'author': "XYZ Company",
    'website': "http://www.xyzcompany.com",

    # Categories can be used to filter modules in modules listing
 
    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ], 
}
