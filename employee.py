from openerp import models, fields

class res_employee(models.Model):

    # Model name
    _name = 'res.employee'
    
    # Model fields
    name = fields.Char(string = 'Name', required = True)
    email_id = fields.Char(string = 'Email Id')
    contact = fields.Char(string = 'Mobile No.')